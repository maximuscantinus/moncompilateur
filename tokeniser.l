%{
// This is our Lexical tokeniser 
// It should be compiled into cpp with :
// flex++ -d -otokeniser.cpp tokeniser.l 
// And then compiled into object with
// g++ -c tokeniser.cpp
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

#include "tokeniser.h"
#include <iostream>

using namespace std;

%}

%option noyywrap
%option c++
%option yylineno

Bool	("True"|"False")
stringconst  \"[^\n"]+\"
ws      [ \t\n\r]+
alpha   [A-Za-z]
float 	[0-9]+\.[0-9]
digit   [0-9]
number  {digit}+
id	{alpha}({alpha}|{digit})*
keyword ("IF"|"THEN"|"ELSE"|"BEGIN"|"END"|"WHILE"|"DO"|"FOR"|"TO"|"DOWNTO"|"DISPLAY"|"VAR"|"BOOLEAN"|"INTEGER"|"DOUBLE"|"CHAR"|"CASE"|"OF"|"STEP")
addop	(\+|\-|\|\|)
mulop	(\*|\/|%|\&\&)
relop	(\<|\>|"=="|\<=|\>=|!=)
unknown [^\"A-Za-z0-9 \n\r\t\(\)\<\>\=\!\%\&\|\}\-\;\.]+
character 	\'\\?.\'

%%

{Bool}		return BOOL;
{addop}		return ADDOP;
{mulop}		return MULOP;
{relop}		return RELOP;
{float}		return FLOAT;
{number}	return NUMBER;
{keyword}	return KEYWORD;
{character}	return CHARACTER;
{id}		return ID;
{stringconst}	return STRINGCONST;
"'" 	return SIMPLEQUOTE;
"["		return RBRACKET;
"]"		return LBRACKET;
","		return COMMA;
";"		return SEMICOLON;
"."		return DOT;
":="	return ASSIGN;
"("		return RPARENT;
")"		return LPARENT;
"!"		return NOT;
":"		return COLON;
<<EOF>>		return FEOF;
{ws}    {/* skip blanks and tabs */};
"(*"    { /* Skip comments between '(*' and '*)' */
		int c;
		while((c = yyinput()) != 0){
     			if(c == '*'){
     	    			if((c = yyinput()) == ')')
    	        			break;
    	     			else
  	          			unput(c);
  	   		}	
		}
	};

{unknown}	return UNKNOWN;

%%

