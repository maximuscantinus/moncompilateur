//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2022 PRAT Cantin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <sstream>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE  {UNSIGNED_INT,BOOLEAN,DOUBLE,CHAR};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string


void IfStatement(void);
void BlockStatement(void);
void WhileStatement(void);
void ForStatement(void);
void DisplayStatement(void);
bool isKey(const char*);
void ReadKey(const char*);
void VarDeclaration(void);
void CaseStatement(void);

map <string, enum TYPE> DeclaredVariables;
map <enum TYPE, string> TypeName = {{UNSIGNED_INT,"INTEGER"}, {BOOLEAN,"BOOLEAN"}, {DOUBLE,"DOUBLE"}, {CHAR,"CHAR"} };
unsigned long TagNumber=0;

bool IsDeclared(const char *id) {
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

bool isCompatible(TYPE a, TYPE b) {
	if (a == b) {
		return true;
	}
	return false;
}

bool isCalc(TYPE a) {
	if (a == UNSIGNED_INT) {
		return true;
	}
	return false;
}

void Error(string s) {
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
// Boolean := "True"|"False"
// Float := Digit.Digit
	
TYPE GetType(void) {
	TYPE type = DeclaredVariables[lexer->YYText()];
	current = (TOKEN) lexer->yylex();
	return type;
}
	
TYPE Identifier(void) {
	cout << "\tpush " << lexer->YYText() << endl;
	TYPE type = DeclaredVariables[lexer->YYText()];
	current = (TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void) {
	cout << "\tpush $" << atoi(lexer->YYText())<<endl;
	current = (TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

TYPE Letter(void) {
	if (current != CHARACTER) 
		Error("charactère attendu");
	cout << "\tmovq $0,%rax" << endl;
	cout << "\tmovb $" << lexer->YYText() << ",%al" << endl;
	cout << "\tpush %rax" << endl;
	current = (TOKEN) lexer->yylex();
	return CHAR;
}

TYPE Float(void) {
	double f = stod(lexer->YYText());  
	long long unsigned int *i;
	i = (long long unsigned int *) &f;

	cout << "\tmovq $" << *i << ", %rax\t# empile le flottant " << f << endl;
	cout << "\tpush %rax" << endl;
	/*cout << "\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top" << endl;
	cout << "\tmovl	$" << *i << ", (%rsp)\t# Conversion of " << f << " (32 bit high part)" << endl;
	cout << "\tmovl	$" << *(i + 1) << ", 4(%rsp)\t# Conversion of " << f << " (32 bit low part)" << endl;
	*/
	current = (TOKEN) lexer->yylex();
	return DOUBLE;
}

TYPE Boolean(void) {
	if (strcmp(lexer->YYText(),"True")) {
		cout << "\tpush $0xFFFFFFFFFFFFFFFF\t\t# True" << endl;
	}
	else if (strcmp(lexer->YYText(),"False")) {
		cout << "\tpush $0\t\t# False" << endl;
	}
	current = (TOKEN) lexer->yylex();
	return BOOLEAN;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE TypeConst(void) {
	TYPE type;
	if (current == FLOAT) {
		type = Float();
	}
	else if (current == NUMBER)
		type = Number();
	else if (current == CHARACTER) {
		type = Letter();
	}
	else if (current == BOOL) {
		type = Boolean();
	}
	else {
		Error("Aucun type trouvé");
	}
	return type;
}

TYPE Factor(void) {
	TYPE type;
	if (current == RPARENT) {
		current = (TOKEN) lexer->yylex();
		type = Expression();
		if (current != LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current = (TOKEN) lexer->yylex();
	}
	else 
		if (current == FLOAT) {
			type = Float();
		}
		else if (current == NUMBER)
			type = Number();
		else if (current == CHARACTER) {
			type = Letter();
		}
		else if (current == BOOL) {
			type = Boolean();
		}
		else {
			if(current == ID)
				type = Identifier();
			else
				Error("'(' ou chiffre ou lettre attendue");
		}
			
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void) {
	OPMUL opmul;
	if (strcmp(lexer->YYText(),"*")==0)
		opmul = MUL;
	else if (strcmp(lexer->YYText(),"/")==0)
		opmul = DIV;
	else if (strcmp(lexer->YYText(),"%")==0)
		opmul = MOD;
	else if (strcmp(lexer->YYText(),"&&")==0)
		opmul = AND;
	else opmul = WTFM;
	current = (TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void) {
	OPMUL mulop;
	TYPE type = Factor();
	TYPE cmpType;
	while (current == MULOP) {
		mulop = MultiplicativeOperator();		// Save operator in local variable
		cmpType = Factor();
		if (!isCompatible(type,cmpType)) {
			stringstream sstm;
			sstm << "Types incompatibles: " << TypeName[type] << " et " << TypeName[cmpType];
			Error(sstm.str());
		}
		if (type != DOUBLE) {
			cout << "\tpop %rbx"<< endl;	// get first operand
			cout << "\tpop %rax"<< endl;	// get second operand
		}
		else {
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
		}
		switch (mulop) {
			case AND:
				if (type == BOOLEAN){
					cout << "\tmulq	%rbx" << endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND" << endl;	// store result
					break;
				}
				else {
					stringstream sstm;
					sstm << "Types incompatibles pour un AND: " << TypeName[type] << " et " << TypeName[cmpType] << ", BOOLEAN was expected";
					Error(sstm.str());
				}
			case MUL:
				if (type != UNSIGNED_INT && type != DOUBLE) {
					stringstream sstm;
					sstm << "Types incompatibles pour un MUL: " << TypeName[type] << ", INTEGER ou DOUBLE was expected";
					Error(sstm.str());
				}
				if (type == UNSIGNED_INT) {
					cout << "\tmulq	%rbx" << endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL" << endl;	// store result
				}
				else if (type == DOUBLE) {
					cout << "\tfmulp	%st(1)" << endl;	// a * b -> %st(1):%st(0)
					cout << "\tsubq $8,%rsp" << endl;	
					cout << "\tfstpl (%rsp)\t# MUL" << endl; // store result
				}
				break;
			case DIV:
				if (type != UNSIGNED_INT && type != DOUBLE) {
					stringstream sstm;
					sstm << "Types incompatibles pour un DIV: " << TypeName[type] << ", INTEGER ou DOUBLE was expected";
					Error(sstm.str());
				}
				if (type == UNSIGNED_INT) {
					cout << "\tmovq $0, %rdx" << endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx" << endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV" << endl;		// store result
				}
				else if (type == DOUBLE) {
					cout << "\tfdivp	%st(1)" << endl;	// a / b -> %st(1):%st(0)
					cout << "\tsubq $8,%rsp" << endl;	
					cout << "\tfstpl (%rsp)\t# DIV" << endl; // store result
				}
				break;
			case MOD:
				if (type != UNSIGNED_INT) {
					stringstream sstm;
					sstm << "Types incompatibles pour un MOD: " << TypeName[type] << ", INTEGER was expected";
					Error(sstm.str());
				}
				cout << "\tmovq $0, %rdx" << endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx" << endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD" << endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void) {
	OPADD opadd;
	if (strcmp(lexer->YYText(),"+") == 0)
		opadd = ADD;
	else if (strcmp(lexer->YYText(),"-") == 0)
		opadd = SUB;
	else if (strcmp(lexer->YYText(),"||") == 0)
		opadd = OR;
	else opadd = WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void) {
	OPADD adop;
	TYPE type = Term();
	TYPE cmpType;
	while (current == ADDOP){
		adop = AdditiveOperator();		// Save operator in local variable
		cmpType = Term();
		if (!isCompatible(type,cmpType)) {
			stringstream sstm;
			sstm << "Types incompatibles: " << TypeName[type] << " et " << TypeName[cmpType];
			Error(sstm.str());
		}
		if (type != DOUBLE) {
			cout << "\tpop %rbx"<< endl;	// get first operand
			cout << "\tpop %rax"<< endl;	// get second operand
		}
		else {
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
		}
		switch (adop){
			case OR:
				if (type == BOOLEAN) {
					cout << "\taddq	%rbx, %rax\t# OR" << endl; // operand1 OR operand2
					cout << "\tpush %rax"<<endl;			// store result
					break;
				}	
				else {
					stringstream sstm;
					sstm << "Types incompatibles pour un OR: " << TypeName[type] << " et " << TypeName[cmpType] << ", BOOLEAN was expected";
					Error(sstm.str());
				}		
			case ADD:
				if (type != UNSIGNED_INT && type != DOUBLE) {
					stringstream sstm;
					sstm << "Types incompatibles pour un ADD: " << TypeName[type] << ", INTEGER was expected";
					Error(sstm.str());
				}
				if (type == UNSIGNED_INT) {
					cout << "\taddq	%rbx, %rax\t# ADD" << endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else if (type == DOUBLE) {
					cout << "\tfaddp	%st(1)\t# ADD" << endl;	// a + b -> %st(1):%st(0)
					cout << "\tsubq $8,%rsp" << endl;	
					cout << "\tfstpl (%rsp)" << endl; // store result
				}
				break;			
			case SUB:	
				if (type != UNSIGNED_INT && type != DOUBLE) {
					stringstream sstm;
					sstm << "Types incompatibles pour un SUB: " << TypeName[type] << ", INTEGER was expected";
					Error(sstm.str());
				}
				if (type == UNSIGNED_INT) {
					cout << "\tsubq	%rbx, %rax\t# SUB" << endl;	// substract both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
 				else if  (type == DOUBLE) {
					cout << "\tfsubp	%st(1)\t# SUB" << endl;	// a - b -> %st(1):%st(0)
					cout << "\tsubq $8,%rsp" << endl;	
					cout << "\tfstpl (%rsp)" << endl; // store result
				}
				break;
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type;
}

TYPE ReadType(void) {
	if (isKey("INTEGER")) {
		current = (TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
	else if (isKey("BOOLEAN")) {
		current = (TOKEN) lexer->yylex();
		return BOOLEAN;
	}
	else if (isKey("DOUBLE")) {
		current = (TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if (isKey("CHAR")) {
		current = (TOKEN) lexer->yylex();
		return CHAR;
	}
	else {
		Error("Aucun type trouvé");
	}
	return UNSIGNED_INT;
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void) {
	ReadKey("VAR");
	cout << "\t.data" << endl;
	cout << "\t.align 8" << endl;
	VarDeclaration();
	while(current == SEMICOLON){
		current = (TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current != DOT)
		Error("caractère '.' attendu");
	cout << "FormatString1:\t.string " << '"' << "%llu\\n" << '"' << "\t# used by printf to display 64-bit unsigned integers" << endl;
	cout << "FormatString2:\t.string " << '"' << "%lf\\n" << '"' << "\t# used by printf to display 64-bit float" << endl;
	cout << "FormatString3:\t.string " << '"' << "%c\\n" << '"' << "\t# used by printf to display 8-bit characters" << endl;
	cout << "FormatStringTrue:\t.string " << '"' << "True\\n" << '"' << "\t# used by printf to True" << endl;
	cout << "FormatStringFalse:\t.string " << '"' << "False\\n" << '"' << "\t# used by printf to True" << endl;
	current=(TOKEN) lexer->yylex();
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void) {
	set<string> idents;
	if(current != ID)
		Error("Un identificateur était attendu");
	idents.insert(lexer->YYText());
	current = (TOKEN) lexer->yylex();
	while(current == COMMA){
		current = (TOKEN) lexer->yylex();
		if(current != ID)
			Error("Un identificateur était attendu");
		idents.insert(lexer->YYText());
		current = (TOKEN) lexer->yylex();
	}
	if (current != COLON) 
		Error("':' attendu");
	current = (TOKEN) lexer->yylex();
	TYPE type = ReadType();
	for (set<string>::iterator it = idents.begin();it != idents.end(); ++it) {
		if (type == DOUBLE) {
			cout << *it << ":\t.double 0" << endl;
		}
		else if (type == CHAR) {
			cout << *it << ":\t.byte 0" << endl;
		} 
		else {
			cout << *it << ":\t.quad 0" << endl;
		}
		DeclaredVariables[*it] = type;
	}
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void) {
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==") == 0)
		oprel = EQU;
	else if(strcmp(lexer->YYText(),"!=") == 0)
		oprel = DIFF;
	else if(strcmp(lexer->YYText(),"<") == 0)
		oprel = INF;
	else if(strcmp(lexer->YYText(),">") == 0)
		oprel = SUP;
	else if(strcmp(lexer->YYText(),"<=") == 0)
		oprel = INFE;
	else if(strcmp(lexer->YYText(),">=") == 0)
		oprel = SUPE;
	else oprel = WTFR;
	current = (TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void) {
	OPREL oprel;
	TYPE type = SimpleExpression();
	TYPE cmpType;
	if(current == RELOP){
		oprel = RelationalOperator();
		cmpType = SimpleExpression();
		if (!isCompatible(type,cmpType)) {
			stringstream sstm;
			sstm << "Types incompatibles: " << TypeName[type] << " et " << TypeName[cmpType];
			Error(sstm.str());
		}
		if (type != DOUBLE) {
			cout << "\tpop %rax" << endl;
			cout << "\tpop %rbx" << endl;
			cout << "\tcmpq %rax, %rbx" << endl;
		}
		else {
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
			cout << "\tfldl (%rsp)"<< endl;
			cout << "\taddq $8, %rsp"<< endl;
			cout << "\tfcomip %st(1)"<< endl;
			cout << "\tfaddp %st(0)\t#On dépile le haut de la pile des flottants après la comparaison" << endl;	
		} 
		switch (oprel) {
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal" << endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different" << endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal" << endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal" << endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below" << endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above" << endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False" << endl;
		cout << "\tjmp Suite" << TagNumber << endl;
		cout << "Vrai" << TagNumber << ":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True" << endl;	
		cout << "Suite" << TagNumber << ":"<<endl;
		return BOOLEAN;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void) { 
	string variable;
	if(current != ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		stringstream sstm;
		sstm << "Erreur : Variable '" << lexer->YYText() << "' non déclarée" << endl;
		Error(sstm.str());
	}
	variable = lexer->YYText();
	TYPE type = GetType();
	if (current != ASSIGN)
		Error("caractères ':=' attendus");
	current = (TOKEN) lexer->yylex();
	TYPE cmpType = Expression();
	if (!isCompatible(type,cmpType)) {
		stringstream sstm;
		sstm << "Types incompatibles: " << TypeName[type] << " et " << TypeName[cmpType];
		Error(sstm.str());
	}
	if (type == CHAR) {
		cout << "\tpop %rax" << endl;
		cout << "\tmovb %al,"<< variable << "\t\t\t\t\t\t#Attribution de la valeur de " << variable << endl;
	}
	else {
		cout << "\tpop " << variable << "\t\t\t\t\t\t#Attribution de la valeur de " << variable << endl;
	}
	return variable;
}

void ReadKey(const char* Key) {
	if (current == KEYWORD && strcmp(lexer->YYText(),Key) == 0) {
		current = (TOKEN) lexer->yylex();
	}
	else {
		Error((string) Key + " attendu");
	}
}

bool isKey(const char* Key) {
	if (current == KEYWORD && strcmp(lexer->YYText(),Key) == 0) {
		return true;
	}
	else {
		return false;
	}
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | CaseStatement
void Statement(void) {
	if (current == ID) {
		AssignementStatement();
	}
	else if (current == KEYWORD) {
		if (isKey("IF")) {
			IfStatement();
		}
		else if (isKey("WHILE")) {
			WhileStatement();
		}
		else if (isKey("FOR")) {
			ForStatement();
		}
		else if (isKey("BEGIN")) {
			BlockStatement();
		}
		else if (isKey("DISPLAY")) {
			DisplayStatement();
		}
		else if (isKey("CASE")) {
			CaseStatement();
		}
		else {
			Error("Aucune instruction trouvée");
		}
	}
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]

void IfStatement(void) {
	ReadKey("IF");
	TYPE type = Expression();
	if (!(type == BOOLEAN)) {
		stringstream sstm;
			sstm << "BOOLEAN attendu, type reçu: " << type;
			Error(sstm.str());
	}
	ReadKey("THEN");
	cout << "\t pop %rax" << endl;
	cout << "\t cmpq $0,%rax \t\t#Condition du IF" << endl;
	cout << "\t je Else" << ++TagNumber << endl;
	unsigned long tmp = TagNumber;
	Statement();
	cout << "\t jmp FinIf" << tmp << endl;
	cout << "Else" << tmp << ":" << endl;
	if (isKey("ELSE")) {
		ReadKey("ELSE");
		Statement();
	}
	cout << "FinIf" << tmp << ":" << endl;
}

// WhileStatement := "WHILE" Expression "DO" Statement

void WhileStatement(void) {
	ReadKey("WHILE");
	cout << "While" << ++TagNumber << ":" << endl;
	unsigned long tmp = TagNumber;
	TYPE type = Expression();
	if (!(type == BOOLEAN)) {
		stringstream sstm;
			sstm << "BOOLEAN attendu, type reçu: " << type;
			Error(sstm.str());
	}
	ReadKey("DO");
	cout << "\t pop %rax" << endl;
	cout << "\t cmpq $0,%rax" << endl;
	cout << "\t je FinWhile" << tmp << endl;
	Statement();
	cout << "\t jmp While" << tmp << endl;
	cout << "FinWhile" << tmp << ":" << endl;
}

// ForStatement := "FOR" AssignementStatement ("TO"|"DOWNTO") Expression ["STEP" Constant] "DO" Statement

void ForStatement(void) {
	ReadKey("FOR");
	string variable = AssignementStatement();
	cout << "For" << ++TagNumber << ":" << endl;
	unsigned long tmp = TagNumber;
	int step = 1;
	if (isKey("TO")) {
		ReadKey("TO");
		Expression();
		if (isKey("STEP")) {
			ReadKey("STEP");
			if (current != NUMBER)
				Error("Constante attendue");
			step = atoi(lexer->YYText());
			current = (TOKEN) lexer->yylex();
		}
		cout << "\tpop %rax" << endl;
		cout << "\tpush " << variable << endl;
		cout << "\tpop %rbx" << endl;
		cout << "\tcmpq %rax,%rbx" << "\t\t#Condition du For" << tmp << " TO" << endl;
		cout << "\tja FinFor" << tmp << endl;
		ReadKey("DO");
		Statement();
		cout << "\tpush " << variable << endl;
		cout << "\tpop %rax" << endl;
		cout << "\taddq $" << step << ",%rax #\t\tOn ajoute " << step << " a la valeur de " << variable  << endl;
		cout << "\tpush %rax"<< endl;
		cout << "\tpop " << variable << endl;
		cout << "\tjmp For" << tmp << endl;
		cout << "FinFor" << tmp << ":" << endl;
	}
	else if (isKey("DOWNTO")) {
		ReadKey("DOWNTO");
		Expression();
		if (isKey("STEP")) {
			ReadKey("STEP");
			if (current != NUMBER)
				Error("Constante attendue");
			step = atoi(lexer->YYText());
			current = (TOKEN) lexer->yylex();
		}
		cout << "\tpop %rax" << endl;
		cout << "\tpush " << variable << endl;
		cout << "\tpop %rbx" << endl;
		cout << "\tcmpq %rax,%rbx" << "\t\t#Condition du For" << tmp << " DOWNTO" << endl;
		cout << "\tjl FinFor" << tmp << endl;
		ReadKey("DO");
		Statement();
		cout << "\tpush " << variable << endl;
		cout << "\tpop %rax" << endl;
		cout << "\tsubq $" << step << ",%rax #\t\tOn retire " << step << " a la valeur de " << variable << endl;
		cout << "\tpush %rax"<< endl;
		cout << "\tpop " << variable << endl;
		cout << "\tjmp For" << tmp << endl;
		cout << "FinFor" << tmp << ":" << endl;
	}
	else {
		Error("TO or DOWNTO expected in IF statement");
	}
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

void BlockStatement(void) {
	ReadKey("BEGIN");
	Statement();
	while (current == SEMICOLON && !isKey("END"))
	{
		current=(TOKEN) lexer->yylex();
		if (!isKey("END")) {
			Statement();
		}
	}
	ReadKey("END");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void) {
	cout << "\t.text\t\t# The following lines contain the program" << endl;
	cout << "\t.globl main\t# The main function must be visible from outside" << endl;
	cout << "main:\t\t\t# The main function body :" << endl;
	cout << "\tpushq %rbp" << endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top" << endl;
	Statement();
	while(current == SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// DisplayStatement := Expression
//Modifié pour correspondre au printf de mon PC
void DisplayStatement(void) {
	ReadKey("DISPLAY");
	TYPE type = Expression();
	if (type == CHAR) {
		cout << "\tpop %rax\t# The value to be displayed" << endl;
		cout << "\tmovq	%rax, %rsi" << endl;
		cout << "\tmovq $FormatString3, %rdi\t#" << '"' << "%\\n" << '"' << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    printf@PLT" << endl;
	}
	else if (type == DOUBLE) {
		cout << "\tpop %rax\t# The value to be displayed" << endl;
		cout << "\tmovq	%rax, %xmm0" << endl;
		cout << "\tmovq $FormatString2, %rdi\t#" << '"' << "%fl\\n" << '"' << endl;
		cout << "\tmovl    $1, %eax" << endl;
		cout << "\tcall    printf@PLT" << endl;
	}
	else if (type == BOOLEAN) {
		++TagNumber;
		unsigned long tmp = TagNumber;
		cout << "\tpop %rax\t# The value to be displayed" << endl;
		cout << "\tmovq	%rax, %rsi" << endl;
		cout << "\tcmpq $0,%rax" << endl;
		cout << "\tje DisplayTrue" << tmp << endl;
		cout << "\tjmp DisplayFalse" << tmp << endl;
		cout << "DisplayTrue" << tmp << ":" << endl;
		cout << "\tmovq $FormatStringTrue, %rdi\t#" << '"' << "True" << '"' << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    printf@PLT" << endl;
		cout << "\tjmp FinDisplay" << tmp << endl;
		cout << "DisplayFalse" << tmp << ":" << endl;
		cout << "\tmovq $FormatStringFalse, %rdi\t#" << '"' << "False" << '"' << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    printf@PLT" << endl;
		cout << "\tjmp FinDisplay" << tmp << endl;
		cout << "FinDisplay" << tmp << ":" << endl;
	}
	else {
		cout << "\tpop %rax\t# The value to be displayed" << endl;
		cout << "\tmovq	%rax, %rsi" << endl;
		cout << "\tmovq $FormatString1, %rdi\t#" << '"' << "%llu\\n" << '"' << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    printf@PLT" << endl;
	}
}

// <case label list> ::= <constant> {, <constant> }
void CaseLabelList(TYPE type) {
	TYPE constType = TypeConst();
	if (type != constType) {
		Error("Types incompatibles");
	}
	cout << "\tpop %rbx" << endl;
	cout << "\tpop %rax" << endl;
	if (type != CHAR) {
		cout << "\tcmpq %rbx,%rax" << endl;
	}
	else {
		cout << "\tcmpb %bl,%al" << endl;
	}
	cout << "\tpush %rax" << endl;
	cout << "\tje Case" << TagNumber << endl;
	while (current == COMMA) {
		current = (TOKEN) lexer->yylex();
		TYPE constType = TypeConst();
		if (type != constType) {
			Error("Types incompatibles");
		}
		cout << "\tpop %rbx" << endl;
		cout << "\tpop %rax" << endl;
		if (type != CHAR) {
			cout << "\tcmpq %rbx,%rax" << endl;
		}
		else {
			cout << "\tcmpb %bl,%al" << endl;
		}
		cout << "\tpush %rax" << endl;
		cout << "\tje Case" << TagNumber << endl;
	}
}

// <case list element> ::= <case label list> : <statement>
void CaseListElement(TYPE type,unsigned long tag) {
	unsigned long tmp = ++TagNumber;
	CaseLabelList(type);
	if (current != COLON) {
		Error("':' attendu");
	}
	current = (TOKEN) lexer->yylex();
	cout << "\tjmp NextCase" << tmp << endl;
	cout << "Case" << tmp << ":" << endl;
	cout << "\taddq $8,%rsp\t#On dépile la valeur a comparer" << endl;
	Statement();
	cout << "\tsubq $8,%rsp" << endl;
	cout << "\tjmp finCase" << tag << endl;
	cout << "NextCase" << tmp << ":" << endl;
}

// <case statement> ::= CASE <expression> OF <case list element> {; <case list element> } [; ELSE <statement>] END
void CaseStatement(void) {
	ReadKey("CASE");
	TYPE type = Expression();
	ReadKey("OF");
	unsigned long tmp = ++TagNumber;
	CaseListElement(type,tmp);
	while (current == SEMICOLON) {
		current = (TOKEN)lexer->yylex();
		if (!isKey("ELSE")) {
			CaseListElement(type,tmp);
		}
	}
	if (isKey("ELSE")) {
		ReadKey("ELSE");
		if (current != COLON) {
			Error("':' attendu");
		}
		current = (TOKEN) lexer->yylex();
		cout << "\taddq $8,%rsp\t#On dépile la valeur a comparer" << endl;
		Statement();
		cout << "\tsubq $8,%rsp" << endl;
		cout << "\tjmp finCase" << tmp << endl;
	}
	cout << "finCase" << tmp << ":" << endl;
	cout << "\taddq $8,%rsp\t#On dépile la valeur a comparer" << endl;
	ReadKey("END");
}

// Program := [VarDeclarationPart] StatementPart
void Program(void) {
	if(isKey("VAR"))
		VarDeclarationPart();
	StatementPart();	
}

int main(void) {	// First version : Source code on standard input and assembly code on standard outputllu\
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler" << endl;
	// Let's proceed to the analysis and code production
	current = (TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tpopq %rbp\t\t# Restore the position of the stack's top" << endl;
	cout << "\tret\t\t\t# Return from main function" << endl;
	if(current!=FEOF){
		cerr << "Caractères en trop à la fin du programme : [" << current << "]";
		Error("."); // unexpected characters at the end of program
	}

}