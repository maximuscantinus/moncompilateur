# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone https://gitlab.com/maximuscantinus/moncompilateur.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**This version Can handle :**

- // Program := [VarDeclarationPart] StatementPart
- // VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
- // VarDeclaration := Ident {"," Ident} ":" Type
- // StatementPart := Statement {";" Statement} "."
- // Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | CaseStatement
- // AssignementStatement := Identifier ":=" Expression
- // IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
- // WhileStatement := "WHILE" Expression "DO" Statement
- // ForStatement := "FOR" AssignementStatement ("TO"|"DOWNTO") Expression ["STEP" Constant] "DO" Statement
- // BlockStatement := "BEGIN" Statement { ";" Statement } "END"
- // DisplayStatement := Expression
- // CaseLabelList ::= constant {, constant }
- // CaseListElement ::= CaseLabelList : Statement
- // CaseStatement ::= CASE Expression OF CaseListElement {; CaseListElement } [; ELSE Statement] END

- // Expression := SimpleExpression [RelationalOperator SimpleExpression]
- // SimpleExpression := Term {AdditiveOperator Term}
- // Term := Factor {MultiplicativeOperator Factor}
- // Factor := Number | Letter | "(" Expression ")"| "!" Factor
- // Number := Digit{Digit}
- // Letter := character
- // Float := float
- // Boolean := Bool
- // Identifier := Letter {(Letter|Digit)}

- // AdditiveOperator := "+" | "-" | "||"
- // MultiplicativeOperator := "*" | "/" | "%" | "&&"
- // RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
- // Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
- // Letter := "a"|...|"z"