			# This code was produced by the CERI Compiler
	.data
	.align 8
a:	.quad 0
b:	.quad 0
c:	.quad 0
d:	.quad 0
f:	.quad 0
v:	.quad 0
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf\n"	# used by printf to display 64-bit float
FormatString3:	.string "%c\n"	# used by printf to display 8-bit characters
FormatStringTrue:	.string "True\n"	# used by printf to True
FormatStringFalse:	.string "False\n"	# used by printf to True
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	pushq %rbp
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0		# False
	pop v						#Attribution de la valeur de v
	push $0xFFFFFFFFFFFFFFFF		# True
	pop f						#Attribution de la valeur de f
	push $10
	pop a						#Attribution de la valeur de a
For1:
	push $0
	pop %rax
	push a
	pop %rbx
	cmpq %rax,%rbx		#Condition du For1 DOWNTO
	jl FinFor1
	push a
	pop %rax	# The value to be displayed
	movq	%rax, %rsi
	movq $FormatString1, %rdi	#"%llu\n"
	movl    $0, %eax
	call    printf@PLT
	push a
	push $3
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rdx	# MOD
	push $0
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai2	# If equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	 pop %rax
	 cmpq $0,%rax 		#Condition du IF
	 je Else3
	push v
	pop %rax	# The value to be displayed
	movq	%rax, %rsi
	cmpq $0,%rax
	je DisplayTrue4
	jmp DisplayFalse4
DisplayTrue4:
	movq $FormatStringTrue, %rdi	#"True"
	movl    $0, %eax
	call    printf@PLT
	jmp FinDisplay4
DisplayFalse4:
	movq $FormatStringFalse, %rdi	#"False"
	movl    $0, %eax
	call    printf@PLT
	jmp FinDisplay4
FinDisplay4:
	 jmp FinIf3
Else3:
	push f
	pop %rax	# The value to be displayed
	movq	%rax, %rsi
	cmpq $0,%rax
	je DisplayTrue5
	jmp DisplayFalse5
DisplayTrue5:
	movq $FormatStringTrue, %rdi	#"True"
	movl    $0, %eax
	call    printf@PLT
	jmp FinDisplay5
DisplayFalse5:
	movq $FormatStringFalse, %rdi	#"False"
	movl    $0, %eax
	call    printf@PLT
	jmp FinDisplay5
FinDisplay5:
FinIf3:
	push a
	pop %rax
	subq $2,%rax #		On retire 2 a la valeur de a
	push %rax
	pop a
	jmp For1
FinFor1:
	popq %rbp		# Restore the position of the stack's top
	ret			# Return from main function
